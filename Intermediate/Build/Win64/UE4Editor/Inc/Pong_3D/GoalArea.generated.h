// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef PONG_3D_GoalArea_generated_h
#error "GoalArea.generated.h already included, missing '#pragma once' in GoalArea.h"
#endif
#define PONG_3D_GoalArea_generated_h

#define Pong_3D_Source_Pong_3D_GoalArea_h_12_SPARSE_DATA
#define Pong_3D_Source_Pong_3D_GoalArea_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOverlapBegin);


#define Pong_3D_Source_Pong_3D_GoalArea_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOverlapBegin);


#define Pong_3D_Source_Pong_3D_GoalArea_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGoalArea(); \
	friend struct Z_Construct_UClass_AGoalArea_Statics; \
public: \
	DECLARE_CLASS(AGoalArea, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pong_3D"), NO_API) \
	DECLARE_SERIALIZER(AGoalArea)


#define Pong_3D_Source_Pong_3D_GoalArea_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAGoalArea(); \
	friend struct Z_Construct_UClass_AGoalArea_Statics; \
public: \
	DECLARE_CLASS(AGoalArea, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pong_3D"), NO_API) \
	DECLARE_SERIALIZER(AGoalArea)


#define Pong_3D_Source_Pong_3D_GoalArea_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGoalArea(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGoalArea) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGoalArea); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGoalArea); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGoalArea(AGoalArea&&); \
	NO_API AGoalArea(const AGoalArea&); \
public:


#define Pong_3D_Source_Pong_3D_GoalArea_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGoalArea(AGoalArea&&); \
	NO_API AGoalArea(const AGoalArea&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGoalArea); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGoalArea); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGoalArea)


#define Pong_3D_Source_Pong_3D_GoalArea_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Trigger() { return STRUCT_OFFSET(AGoalArea, Trigger); } \
	FORCEINLINE static uint32 __PPO__OponentID() { return STRUCT_OFFSET(AGoalArea, OponentID); }


#define Pong_3D_Source_Pong_3D_GoalArea_h_9_PROLOG
#define Pong_3D_Source_Pong_3D_GoalArea_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pong_3D_Source_Pong_3D_GoalArea_h_12_PRIVATE_PROPERTY_OFFSET \
	Pong_3D_Source_Pong_3D_GoalArea_h_12_SPARSE_DATA \
	Pong_3D_Source_Pong_3D_GoalArea_h_12_RPC_WRAPPERS \
	Pong_3D_Source_Pong_3D_GoalArea_h_12_INCLASS \
	Pong_3D_Source_Pong_3D_GoalArea_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pong_3D_Source_Pong_3D_GoalArea_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pong_3D_Source_Pong_3D_GoalArea_h_12_PRIVATE_PROPERTY_OFFSET \
	Pong_3D_Source_Pong_3D_GoalArea_h_12_SPARSE_DATA \
	Pong_3D_Source_Pong_3D_GoalArea_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Pong_3D_Source_Pong_3D_GoalArea_h_12_INCLASS_NO_PURE_DECLS \
	Pong_3D_Source_Pong_3D_GoalArea_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PONG_3D_API UClass* StaticClass<class AGoalArea>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pong_3D_Source_Pong_3D_GoalArea_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
