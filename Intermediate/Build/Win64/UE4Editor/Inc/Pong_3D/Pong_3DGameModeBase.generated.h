// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PONG_3D_Pong_3DGameModeBase_generated_h
#error "Pong_3DGameModeBase.generated.h already included, missing '#pragma once' in Pong_3DGameModeBase.h"
#endif
#define PONG_3D_Pong_3DGameModeBase_generated_h

#define Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_SPARSE_DATA
#define Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetMaxScore); \
	DECLARE_FUNCTION(execGetMaxSore); \
	DECLARE_FUNCTION(execResetScore); \
	DECLARE_FUNCTION(execGetPlayerScore); \
	DECLARE_FUNCTION(execIncreasePlayerScore); \
	DECLARE_FUNCTION(execSpawnTheBall);


#define Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetMaxScore); \
	DECLARE_FUNCTION(execGetMaxSore); \
	DECLARE_FUNCTION(execResetScore); \
	DECLARE_FUNCTION(execGetPlayerScore); \
	DECLARE_FUNCTION(execIncreasePlayerScore); \
	DECLARE_FUNCTION(execSpawnTheBall);


#define Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPong_3DGameModeBase(); \
	friend struct Z_Construct_UClass_APong_3DGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APong_3DGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pong_3D"), NO_API) \
	DECLARE_SERIALIZER(APong_3DGameModeBase)


#define Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_INCLASS \
private: \
	static void StaticRegisterNativesAPong_3DGameModeBase(); \
	friend struct Z_Construct_UClass_APong_3DGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APong_3DGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pong_3D"), NO_API) \
	DECLARE_SERIALIZER(APong_3DGameModeBase)


#define Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APong_3DGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APong_3DGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APong_3DGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APong_3DGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APong_3DGameModeBase(APong_3DGameModeBase&&); \
	NO_API APong_3DGameModeBase(const APong_3DGameModeBase&); \
public:


#define Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APong_3DGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APong_3DGameModeBase(APong_3DGameModeBase&&); \
	NO_API APong_3DGameModeBase(const APong_3DGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APong_3DGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APong_3DGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APong_3DGameModeBase)


#define Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__P1_Score() { return STRUCT_OFFSET(APong_3DGameModeBase, P1_Score); } \
	FORCEINLINE static uint32 __PPO__P2_Score() { return STRUCT_OFFSET(APong_3DGameModeBase, P2_Score); } \
	FORCEINLINE static uint32 __PPO__MaxScore() { return STRUCT_OFFSET(APong_3DGameModeBase, MaxScore); } \
	FORCEINLINE static uint32 __PPO__IsAllPlayersConnected() { return STRUCT_OFFSET(APong_3DGameModeBase, IsAllPlayersConnected); }


#define Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_16_PROLOG
#define Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_PRIVATE_PROPERTY_OFFSET \
	Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_SPARSE_DATA \
	Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_RPC_WRAPPERS \
	Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_INCLASS \
	Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_PRIVATE_PROPERTY_OFFSET \
	Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_SPARSE_DATA \
	Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_INCLASS_NO_PURE_DECLS \
	Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PONG_3D_API UClass* StaticClass<class APong_3DGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pong_3D_Source_Pong_3D_Pong_3DGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
