// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class APlayerStart;
#ifdef PONG_3D_PlayerPaddle_generated_h
#error "PlayerPaddle.generated.h already included, missing '#pragma once' in PlayerPaddle.h"
#endif
#define PONG_3D_PlayerPaddle_generated_h

#define Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_SPARSE_DATA
#define Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execMoveRight); \
	DECLARE_FUNCTION(execFindNearestPlayerStart); \
	DECLARE_FUNCTION(execOnRep_PaddleLocation);


#define Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execMoveRight); \
	DECLARE_FUNCTION(execFindNearestPlayerStart); \
	DECLARE_FUNCTION(execOnRep_PaddleLocation);


#define Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerPaddle(); \
	friend struct Z_Construct_UClass_APlayerPaddle_Statics; \
public: \
	DECLARE_CLASS(APlayerPaddle, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pong_3D"), NO_API) \
	DECLARE_SERIALIZER(APlayerPaddle) \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		PaddleLocation=NETFIELD_REP_START, \
		NETFIELD_REP_END=PaddleLocation	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerPaddle(); \
	friend struct Z_Construct_UClass_APlayerPaddle_Statics; \
public: \
	DECLARE_CLASS(APlayerPaddle, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pong_3D"), NO_API) \
	DECLARE_SERIALIZER(APlayerPaddle) \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		PaddleLocation=NETFIELD_REP_START, \
		NETFIELD_REP_END=PaddleLocation	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerPaddle(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerPaddle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPaddle); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPaddle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPaddle(APlayerPaddle&&); \
	NO_API APlayerPaddle(const APlayerPaddle&); \
public:


#define Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPaddle(APlayerPaddle&&); \
	NO_API APlayerPaddle(const APlayerPaddle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPaddle); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPaddle); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerPaddle)


#define Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PaddleSpeed() { return STRUCT_OFFSET(APlayerPaddle, PaddleSpeed); } \
	FORCEINLINE static uint32 __PPO__MeshHeigth() { return STRUCT_OFFSET(APlayerPaddle, MeshHeigth); } \
	FORCEINLINE static uint32 __PPO__PaddleWidth() { return STRUCT_OFFSET(APlayerPaddle, PaddleWidth); } \
	FORCEINLINE static uint32 __PPO__PaddleLocation() { return STRUCT_OFFSET(APlayerPaddle, PaddleLocation); } \
	FORCEINLINE static uint32 __PPO__Paddle() { return STRUCT_OFFSET(APlayerPaddle, Paddle); } \
	FORCEINLINE static uint32 __PPO__Camera() { return STRUCT_OFFSET(APlayerPaddle, Camera); }


#define Pong_3D_Source_Pong_3D_PlayerPaddle_h_14_PROLOG
#define Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_PRIVATE_PROPERTY_OFFSET \
	Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_SPARSE_DATA \
	Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_RPC_WRAPPERS \
	Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_INCLASS \
	Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_PRIVATE_PROPERTY_OFFSET \
	Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_SPARSE_DATA \
	Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_INCLASS_NO_PURE_DECLS \
	Pong_3D_Source_Pong_3D_PlayerPaddle_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PONG_3D_API UClass* StaticClass<class APlayerPaddle>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pong_3D_Source_Pong_3D_PlayerPaddle_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
