// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Pong_3D/Pong_3DGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePong_3DGameModeBase() {}
// Cross Module References
	PONG_3D_API UClass* Z_Construct_UClass_APong_3DGameModeBase_NoRegister();
	PONG_3D_API UClass* Z_Construct_UClass_APong_3DGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Pong_3D();
// End Cross Module References
	DEFINE_FUNCTION(APong_3DGameModeBase::execSetMaxScore)
	{
		P_GET_PROPERTY(FByteProperty,Z_Param_NewMaxScore);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetMaxScore(Z_Param_NewMaxScore);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APong_3DGameModeBase::execGetMaxSore)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(uint8*)Z_Param__Result=P_THIS->GetMaxSore();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APong_3DGameModeBase::execResetScore)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetScore();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APong_3DGameModeBase::execGetPlayerScore)
	{
		P_GET_PROPERTY(FByteProperty,Z_Param_PlayerID);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(uint8*)Z_Param__Result=P_THIS->GetPlayerScore(Z_Param_PlayerID);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APong_3DGameModeBase::execIncreasePlayerScore)
	{
		P_GET_PROPERTY(FByteProperty,Z_Param_PlayerID);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->IncreasePlayerScore(Z_Param_PlayerID);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APong_3DGameModeBase::execSpawnTheBall)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnTheBall();
		P_NATIVE_END;
	}
	void APong_3DGameModeBase::StaticRegisterNativesAPong_3DGameModeBase()
	{
		UClass* Class = APong_3DGameModeBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetMaxSore", &APong_3DGameModeBase::execGetMaxSore },
			{ "GetPlayerScore", &APong_3DGameModeBase::execGetPlayerScore },
			{ "IncreasePlayerScore", &APong_3DGameModeBase::execIncreasePlayerScore },
			{ "ResetScore", &APong_3DGameModeBase::execResetScore },
			{ "SetMaxScore", &APong_3DGameModeBase::execSetMaxScore },
			{ "SpawnTheBall", &APong_3DGameModeBase::execSpawnTheBall },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APong_3DGameModeBase_GetMaxSore_Statics
	{
		struct Pong_3DGameModeBase_eventGetMaxSore_Parms
		{
			uint8 ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_APong_3DGameModeBase_GetMaxSore_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pong_3DGameModeBase_eventGetMaxSore_Parms, ReturnValue), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APong_3DGameModeBase_GetMaxSore_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APong_3DGameModeBase_GetMaxSore_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APong_3DGameModeBase_GetMaxSore_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Called to get Maximum score\n" },
		{ "ModuleRelativePath", "Pong_3DGameModeBase.h" },
		{ "ToolTip", "Called to get Maximum score" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APong_3DGameModeBase_GetMaxSore_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APong_3DGameModeBase, nullptr, "GetMaxSore", nullptr, nullptr, sizeof(Pong_3DGameModeBase_eventGetMaxSore_Parms), Z_Construct_UFunction_APong_3DGameModeBase_GetMaxSore_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APong_3DGameModeBase_GetMaxSore_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APong_3DGameModeBase_GetMaxSore_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APong_3DGameModeBase_GetMaxSore_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APong_3DGameModeBase_GetMaxSore()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APong_3DGameModeBase_GetMaxSore_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore_Statics
	{
		struct Pong_3DGameModeBase_eventGetPlayerScore_Parms
		{
			uint8 PlayerID;
			uint8 ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PlayerID;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore_Statics::NewProp_PlayerID = { "PlayerID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pong_3DGameModeBase_eventGetPlayerScore_Parms, PlayerID), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pong_3DGameModeBase_eventGetPlayerScore_Parms, ReturnValue), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore_Statics::NewProp_PlayerID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Called to get player score\n" },
		{ "ModuleRelativePath", "Pong_3DGameModeBase.h" },
		{ "ToolTip", "Called to get player score" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APong_3DGameModeBase, nullptr, "GetPlayerScore", nullptr, nullptr, sizeof(Pong_3DGameModeBase_eventGetPlayerScore_Parms), Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APong_3DGameModeBase_IncreasePlayerScore_Statics
	{
		struct Pong_3DGameModeBase_eventIncreasePlayerScore_Parms
		{
			uint8 PlayerID;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PlayerID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_APong_3DGameModeBase_IncreasePlayerScore_Statics::NewProp_PlayerID = { "PlayerID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pong_3DGameModeBase_eventIncreasePlayerScore_Parms, PlayerID), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APong_3DGameModeBase_IncreasePlayerScore_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APong_3DGameModeBase_IncreasePlayerScore_Statics::NewProp_PlayerID,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APong_3DGameModeBase_IncreasePlayerScore_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Called to increase player score\n" },
		{ "ModuleRelativePath", "Pong_3DGameModeBase.h" },
		{ "ToolTip", "Called to increase player score" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APong_3DGameModeBase_IncreasePlayerScore_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APong_3DGameModeBase, nullptr, "IncreasePlayerScore", nullptr, nullptr, sizeof(Pong_3DGameModeBase_eventIncreasePlayerScore_Parms), Z_Construct_UFunction_APong_3DGameModeBase_IncreasePlayerScore_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APong_3DGameModeBase_IncreasePlayerScore_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APong_3DGameModeBase_IncreasePlayerScore_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APong_3DGameModeBase_IncreasePlayerScore_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APong_3DGameModeBase_IncreasePlayerScore()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APong_3DGameModeBase_IncreasePlayerScore_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APong_3DGameModeBase_ResetScore_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APong_3DGameModeBase_ResetScore_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Pong_3DGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APong_3DGameModeBase_ResetScore_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APong_3DGameModeBase, nullptr, "ResetScore", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APong_3DGameModeBase_ResetScore_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APong_3DGameModeBase_ResetScore_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APong_3DGameModeBase_ResetScore()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APong_3DGameModeBase_ResetScore_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APong_3DGameModeBase_SetMaxScore_Statics
	{
		struct Pong_3DGameModeBase_eventSetMaxScore_Parms
		{
			uint8 NewMaxScore;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_NewMaxScore;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_APong_3DGameModeBase_SetMaxScore_Statics::NewProp_NewMaxScore = { "NewMaxScore", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pong_3DGameModeBase_eventSetMaxScore_Parms, NewMaxScore), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APong_3DGameModeBase_SetMaxScore_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APong_3DGameModeBase_SetMaxScore_Statics::NewProp_NewMaxScore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APong_3DGameModeBase_SetMaxScore_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Called to set maximum score\n" },
		{ "ModuleRelativePath", "Pong_3DGameModeBase.h" },
		{ "ToolTip", "Called to set maximum score" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APong_3DGameModeBase_SetMaxScore_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APong_3DGameModeBase, nullptr, "SetMaxScore", nullptr, nullptr, sizeof(Pong_3DGameModeBase_eventSetMaxScore_Parms), Z_Construct_UFunction_APong_3DGameModeBase_SetMaxScore_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APong_3DGameModeBase_SetMaxScore_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APong_3DGameModeBase_SetMaxScore_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APong_3DGameModeBase_SetMaxScore_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APong_3DGameModeBase_SetMaxScore()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APong_3DGameModeBase_SetMaxScore_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APong_3DGameModeBase_SpawnTheBall_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APong_3DGameModeBase_SpawnTheBall_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Called to spawn the ball in the center\n// (usually to begin the round)\n" },
		{ "ModuleRelativePath", "Pong_3DGameModeBase.h" },
		{ "ToolTip", "Called to spawn the ball in the center\n(usually to begin the round)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APong_3DGameModeBase_SpawnTheBall_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APong_3DGameModeBase, nullptr, "SpawnTheBall", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APong_3DGameModeBase_SpawnTheBall_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APong_3DGameModeBase_SpawnTheBall_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APong_3DGameModeBase_SpawnTheBall()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APong_3DGameModeBase_SpawnTheBall_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APong_3DGameModeBase_NoRegister()
	{
		return APong_3DGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_APong_3DGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_P1_Score_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_P1_Score;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_P2_Score_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_P2_Score;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxScore_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MaxScore;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsAllPlayersConnected_MetaData[];
#endif
		static void NewProp_IsAllPlayersConnected_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsAllPlayersConnected;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APong_3DGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Pong_3D,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APong_3DGameModeBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APong_3DGameModeBase_GetMaxSore, "GetMaxSore" }, // 3157640923
		{ &Z_Construct_UFunction_APong_3DGameModeBase_GetPlayerScore, "GetPlayerScore" }, // 2939891198
		{ &Z_Construct_UFunction_APong_3DGameModeBase_IncreasePlayerScore, "IncreasePlayerScore" }, // 3276471518
		{ &Z_Construct_UFunction_APong_3DGameModeBase_ResetScore, "ResetScore" }, // 476829182
		{ &Z_Construct_UFunction_APong_3DGameModeBase_SetMaxScore, "SetMaxScore" }, // 1548011232
		{ &Z_Construct_UFunction_APong_3DGameModeBase_SpawnTheBall, "SpawnTheBall" }, // 70610043
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APong_3DGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Pong_3DGameModeBase.h" },
		{ "ModuleRelativePath", "Pong_3DGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_P1_Score_MetaData[] = {
		{ "Category", "Pong_3DGameModeBase" },
		{ "Comment", "// First Player Score\n" },
		{ "ModuleRelativePath", "Pong_3DGameModeBase.h" },
		{ "ToolTip", "First Player Score" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_P1_Score = { "P1_Score", nullptr, (EPropertyFlags)0x0020080000020015, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APong_3DGameModeBase, P1_Score), nullptr, METADATA_PARAMS(Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_P1_Score_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_P1_Score_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_P2_Score_MetaData[] = {
		{ "Category", "Pong_3DGameModeBase" },
		{ "Comment", "// Second player score\n" },
		{ "ModuleRelativePath", "Pong_3DGameModeBase.h" },
		{ "ToolTip", "Second player score" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_P2_Score = { "P2_Score", nullptr, (EPropertyFlags)0x0020080000020015, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APong_3DGameModeBase, P2_Score), nullptr, METADATA_PARAMS(Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_P2_Score_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_P2_Score_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_MaxScore_MetaData[] = {
		{ "Category", "Pong_3DGameModeBase" },
		{ "Comment", "// Maximum score\n" },
		{ "ModuleRelativePath", "Pong_3DGameModeBase.h" },
		{ "ToolTip", "Maximum score" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_MaxScore = { "MaxScore", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APong_3DGameModeBase, MaxScore), nullptr, METADATA_PARAMS(Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_MaxScore_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_MaxScore_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_IsAllPlayersConnected_MetaData[] = {
		{ "Category", "Pong_3DGameModeBase" },
		{ "Comment", "// Represent status if all players are connected\n" },
		{ "ModuleRelativePath", "Pong_3DGameModeBase.h" },
		{ "ToolTip", "Represent status if all players are connected" },
	};
#endif
	void Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_IsAllPlayersConnected_SetBit(void* Obj)
	{
		((APong_3DGameModeBase*)Obj)->IsAllPlayersConnected = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_IsAllPlayersConnected = { "IsAllPlayersConnected", nullptr, (EPropertyFlags)0x0020080000020015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(APong_3DGameModeBase), &Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_IsAllPlayersConnected_SetBit, METADATA_PARAMS(Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_IsAllPlayersConnected_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_IsAllPlayersConnected_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APong_3DGameModeBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_P1_Score,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_P2_Score,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_MaxScore,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APong_3DGameModeBase_Statics::NewProp_IsAllPlayersConnected,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APong_3DGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APong_3DGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APong_3DGameModeBase_Statics::ClassParams = {
		&APong_3DGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APong_3DGameModeBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_APong_3DGameModeBase_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_APong_3DGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APong_3DGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APong_3DGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APong_3DGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APong_3DGameModeBase, 1393914504);
	template<> PONG_3D_API UClass* StaticClass<APong_3DGameModeBase>()
	{
		return APong_3DGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APong_3DGameModeBase(Z_Construct_UClass_APong_3DGameModeBase, &APong_3DGameModeBase::StaticClass, TEXT("/Script/Pong_3D"), TEXT("APong_3DGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APong_3DGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
