// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
#ifdef PONG_3D_Ball_generated_h
#error "Ball.generated.h already included, missing '#pragma once' in Ball.h"
#endif
#define PONG_3D_Ball_generated_h

#define Pong_3D_Source_Pong_3D_Ball_h_15_SPARSE_DATA
#define Pong_3D_Source_Pong_3D_Ball_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetRandDirection);


#define Pong_3D_Source_Pong_3D_Ball_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetRandDirection);


#define Pong_3D_Source_Pong_3D_Ball_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABall(); \
	friend struct Z_Construct_UClass_ABall_Statics; \
public: \
	DECLARE_CLASS(ABall, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pong_3D"), NO_API) \
	DECLARE_SERIALIZER(ABall)


#define Pong_3D_Source_Pong_3D_Ball_h_15_INCLASS \
private: \
	static void StaticRegisterNativesABall(); \
	friend struct Z_Construct_UClass_ABall_Statics; \
public: \
	DECLARE_CLASS(ABall, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pong_3D"), NO_API) \
	DECLARE_SERIALIZER(ABall)


#define Pong_3D_Source_Pong_3D_Ball_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABall(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABall) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABall); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABall); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABall(ABall&&); \
	NO_API ABall(const ABall&); \
public:


#define Pong_3D_Source_Pong_3D_Ball_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABall(ABall&&); \
	NO_API ABall(const ABall&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABall); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABall); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABall)


#define Pong_3D_Source_Pong_3D_Ball_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BallCollision() { return STRUCT_OFFSET(ABall, BallCollision); } \
	FORCEINLINE static uint32 __PPO__BallRadius() { return STRUCT_OFFSET(ABall, BallRadius); } \
	FORCEINLINE static uint32 __PPO__BallDefaultHalfHeigth() { return STRUCT_OFFSET(ABall, BallDefaultHalfHeigth); } \
	FORCEINLINE static uint32 __PPO__BallMovement() { return STRUCT_OFFSET(ABall, BallMovement); } \
	FORCEINLINE static uint32 __PPO__BallSpeed() { return STRUCT_OFFSET(ABall, BallSpeed); } \
	FORCEINLINE static uint32 __PPO__BallMaterial() { return STRUCT_OFFSET(ABall, BallMaterial); } \
	FORCEINLINE static uint32 __PPO__BallStartMinAngle() { return STRUCT_OFFSET(ABall, BallStartMinAngle); } \
	FORCEINLINE static uint32 __PPO__BallStartMaxAngle() { return STRUCT_OFFSET(ABall, BallStartMaxAngle); }


#define Pong_3D_Source_Pong_3D_Ball_h_12_PROLOG
#define Pong_3D_Source_Pong_3D_Ball_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pong_3D_Source_Pong_3D_Ball_h_15_PRIVATE_PROPERTY_OFFSET \
	Pong_3D_Source_Pong_3D_Ball_h_15_SPARSE_DATA \
	Pong_3D_Source_Pong_3D_Ball_h_15_RPC_WRAPPERS \
	Pong_3D_Source_Pong_3D_Ball_h_15_INCLASS \
	Pong_3D_Source_Pong_3D_Ball_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pong_3D_Source_Pong_3D_Ball_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pong_3D_Source_Pong_3D_Ball_h_15_PRIVATE_PROPERTY_OFFSET \
	Pong_3D_Source_Pong_3D_Ball_h_15_SPARSE_DATA \
	Pong_3D_Source_Pong_3D_Ball_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Pong_3D_Source_Pong_3D_Ball_h_15_INCLASS_NO_PURE_DECLS \
	Pong_3D_Source_Pong_3D_Ball_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PONG_3D_API UClass* StaticClass<class ABall>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pong_3D_Source_Pong_3D_Ball_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
