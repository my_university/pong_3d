// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Pong_3D/PlayerPaddle.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlayerPaddle() {}
// Cross Module References
	PONG_3D_API UClass* Z_Construct_UClass_APlayerPaddle_NoRegister();
	PONG_3D_API UClass* Z_Construct_UClass_APlayerPaddle();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_Pong_3D();
	ENGINE_API UClass* Z_Construct_UClass_APlayerStart_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(APlayerPaddle::execMoveRight)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_AxisValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->MoveRight(Z_Param_AxisValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APlayerPaddle::execFindNearestPlayerStart)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(APlayerStart**)Z_Param__Result=P_THIS->FindNearestPlayerStart();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APlayerPaddle::execOnRep_PaddleLocation)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnRep_PaddleLocation();
		P_NATIVE_END;
	}
	void APlayerPaddle::StaticRegisterNativesAPlayerPaddle()
	{
		UClass* Class = APlayerPaddle::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "FindNearestPlayerStart", &APlayerPaddle::execFindNearestPlayerStart },
			{ "MoveRight", &APlayerPaddle::execMoveRight },
			{ "OnRep_PaddleLocation", &APlayerPaddle::execOnRep_PaddleLocation },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APlayerPaddle_FindNearestPlayerStart_Statics
	{
		struct PlayerPaddle_eventFindNearestPlayerStart_Parms
		{
			APlayerStart* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APlayerPaddle_FindNearestPlayerStart_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PlayerPaddle_eventFindNearestPlayerStart_Parms, ReturnValue), Z_Construct_UClass_APlayerStart_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APlayerPaddle_FindNearestPlayerStart_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APlayerPaddle_FindNearestPlayerStart_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlayerPaddle_FindNearestPlayerStart_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Find the nearest player start\n" },
		{ "ModuleRelativePath", "PlayerPaddle.h" },
		{ "ToolTip", "Find the nearest player start" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlayerPaddle_FindNearestPlayerStart_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlayerPaddle, nullptr, "FindNearestPlayerStart", nullptr, nullptr, sizeof(PlayerPaddle_eventFindNearestPlayerStart_Parms), Z_Construct_UFunction_APlayerPaddle_FindNearestPlayerStart_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayerPaddle_FindNearestPlayerStart_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlayerPaddle_FindNearestPlayerStart_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayerPaddle_FindNearestPlayerStart_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlayerPaddle_FindNearestPlayerStart()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlayerPaddle_FindNearestPlayerStart_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlayerPaddle_MoveRight_Statics
	{
		struct PlayerPaddle_eventMoveRight_Parms
		{
			float AxisValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AxisValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_APlayerPaddle_MoveRight_Statics::NewProp_AxisValue = { "AxisValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PlayerPaddle_eventMoveRight_Parms, AxisValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APlayerPaddle_MoveRight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APlayerPaddle_MoveRight_Statics::NewProp_AxisValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlayerPaddle_MoveRight_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/*\n\x09*/// Called on MoveRight axis event\n" },
		{ "ModuleRelativePath", "PlayerPaddle.h" },
		{ "ToolTip", "// Called on MoveRight axis event" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlayerPaddle_MoveRight_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlayerPaddle, nullptr, "MoveRight", nullptr, nullptr, sizeof(PlayerPaddle_eventMoveRight_Parms), Z_Construct_UFunction_APlayerPaddle_MoveRight_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayerPaddle_MoveRight_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlayerPaddle_MoveRight_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayerPaddle_MoveRight_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlayerPaddle_MoveRight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlayerPaddle_MoveRight_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlayerPaddle_OnRep_PaddleLocation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlayerPaddle_OnRep_PaddleLocation_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Replicate the paddle's location to clients\n" },
		{ "ModuleRelativePath", "PlayerPaddle.h" },
		{ "ToolTip", "Replicate the paddle's location to clients" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlayerPaddle_OnRep_PaddleLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlayerPaddle, nullptr, "OnRep_PaddleLocation", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlayerPaddle_OnRep_PaddleLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayerPaddle_OnRep_PaddleLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlayerPaddle_OnRep_PaddleLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlayerPaddle_OnRep_PaddleLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APlayerPaddle_NoRegister()
	{
		return APlayerPaddle::StaticClass();
	}
	struct Z_Construct_UClass_APlayerPaddle_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaddleSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PaddleSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshHeigth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MeshHeigth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaddleWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PaddleWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaddleLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PaddleLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Paddle_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Paddle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Camera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Camera;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APlayerPaddle_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
		(UObject* (*)())Z_Construct_UPackage__Script_Pong_3D,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APlayerPaddle_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APlayerPaddle_FindNearestPlayerStart, "FindNearestPlayerStart" }, // 3830493438
		{ &Z_Construct_UFunction_APlayerPaddle_MoveRight, "MoveRight" }, // 1806930234
		{ &Z_Construct_UFunction_APlayerPaddle_OnRep_PaddleLocation, "OnRep_PaddleLocation" }, // 2390804584
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerPaddle_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "PlayerPaddle.h" },
		{ "ModuleRelativePath", "PlayerPaddle.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleSpeed_MetaData[] = {
		{ "Category", "PlayerPaddle" },
		{ "Comment", "// The speed of the paddle\n" },
		{ "ModuleRelativePath", "PlayerPaddle.h" },
		{ "ToolTip", "The speed of the paddle" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleSpeed = { "PaddleSpeed", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerPaddle, PaddleSpeed), METADATA_PARAMS(Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerPaddle_Statics::NewProp_MeshHeigth_MetaData[] = {
		{ "Category", "PlayerPaddle" },
		{ "Comment", "// Default mesh height\n" },
		{ "ModuleRelativePath", "PlayerPaddle.h" },
		{ "ToolTip", "Default mesh height" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlayerPaddle_Statics::NewProp_MeshHeigth = { "MeshHeigth", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerPaddle, MeshHeigth), METADATA_PARAMS(Z_Construct_UClass_APlayerPaddle_Statics::NewProp_MeshHeigth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerPaddle_Statics::NewProp_MeshHeigth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleWidth_MetaData[] = {
		{ "Category", "PlayerPaddle" },
		{ "Comment", "// Paddle width\n" },
		{ "ModuleRelativePath", "PlayerPaddle.h" },
		{ "ToolTip", "Paddle width" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleWidth = { "PaddleWidth", nullptr, (EPropertyFlags)0x0040000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerPaddle, PaddleWidth), METADATA_PARAMS(Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleLocation_MetaData[] = {
		{ "Comment", "// Paddle location\n" },
		{ "ModuleRelativePath", "PlayerPaddle.h" },
		{ "ToolTip", "Paddle location" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleLocation = { "PaddleLocation", nullptr, (EPropertyFlags)0x0040000000000020, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerPaddle, PaddleLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerPaddle_Statics::NewProp_Paddle_MetaData[] = {
		{ "Category", "PlayerPaddle" },
		{ "Comment", "// Player's paddle\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "PlayerPaddle.h" },
		{ "ToolTip", "Player's paddle" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APlayerPaddle_Statics::NewProp_Paddle = { "Paddle", nullptr, (EPropertyFlags)0x0020080000090009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerPaddle, Paddle), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APlayerPaddle_Statics::NewProp_Paddle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerPaddle_Statics::NewProp_Paddle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerPaddle_Statics::NewProp_Camera_MetaData[] = {
		{ "Category", "PlayerPaddle" },
		{ "Comment", "// Player's Camera\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "PlayerPaddle.h" },
		{ "ToolTip", "Player's Camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APlayerPaddle_Statics::NewProp_Camera = { "Camera", nullptr, (EPropertyFlags)0x0020080000090009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerPaddle, Camera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APlayerPaddle_Statics::NewProp_Camera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerPaddle_Statics::NewProp_Camera_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APlayerPaddle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerPaddle_Statics::NewProp_MeshHeigth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerPaddle_Statics::NewProp_PaddleLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerPaddle_Statics::NewProp_Paddle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerPaddle_Statics::NewProp_Camera,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APlayerPaddle_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APlayerPaddle>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APlayerPaddle_Statics::ClassParams = {
		&APlayerPaddle::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APlayerPaddle_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_APlayerPaddle_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APlayerPaddle_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerPaddle_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APlayerPaddle()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APlayerPaddle_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APlayerPaddle, 2694733708);
	template<> PONG_3D_API UClass* StaticClass<APlayerPaddle>()
	{
		return APlayerPaddle::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APlayerPaddle(Z_Construct_UClass_APlayerPaddle, &APlayerPaddle::StaticClass, TEXT("/Script/Pong_3D"), TEXT("APlayerPaddle"), false, nullptr, nullptr, nullptr);

	void APlayerPaddle::ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const
	{
		static const FName Name_PaddleLocation(TEXT("PaddleLocation"));

		const bool bIsValid = true
			&& Name_PaddleLocation == ClassReps[(int32)ENetFields_Private::PaddleLocation].Property->GetFName();

		checkf(bIsValid, TEXT("UHT Generated Rep Indices do not match runtime populated Rep Indices for properties in APlayerPaddle"));
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlayerPaddle);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
