// Fill out your copyright notice in the Description page of Project Settings.


#include "Ball.h"

#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ABall::ABall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetReplicates(true);
	SetReplicateMovement(true);

	// Create default RootComponent
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	// Create default ball
	BallCollision = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Ball"));
	BallMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("BallMovement"));

	// Init ball
	const ConstructorHelpers::FObjectFinder<UStaticMesh> BallMesh(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));
	if (BallMesh.Succeeded()) {
		BallCollision->SetStaticMesh(BallMesh.Object);
	}

	// Add material to the ball
	static ConstructorHelpers::FObjectFinder<UMaterialInstance> FoundMaterial(TEXT("/Engine/VREditor/UI/DiscMaterial.DiscMaterial"));
	if (FoundMaterial.Succeeded()) {
		BallMaterial = FoundMaterial.Object;
		BallCollision->SetMaterial(0, BallMaterial);
	}

	// Resize ball and add z-axis shift
	BallCollision->SetRelativeScale3D(FVector(BallRadius));
	BallCollision->SetRelativeLocation(FVector(0.0f, 0.0f, 50.0f * BallRadius));
	
	BallMovement->SetUpdatedComponent(BallCollision);
	
	// Set bouncing true
	BallMovement->bShouldBounce = true;

	// Remove velocity loss 
	BallMovement->Bounciness = 1.0f;
	BallMovement->Friction = 0.0f;

	BallMovement->ProjectileGravityScale = 0.0f;
	BallMovement->SetIsReplicated(true);

	// Attach to RootComponent
	BallCollision->SetupAttachment(RootComponent);
	BallCollision->SetIsReplicated(true);

}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();

	// Add speed
	BallMovement->InitialSpeed = BallSpeed;
	BallMovement->MaxSpeed = BallSpeed;
	
	// Resize ball and add shift in case BallRadius was changed
	BallCollision->SetRelativeScale3D(FVector(BallRadius));
	BallCollision->SetRelativeLocation(FVector(0.0f, 0.0f, BallDefaultHalfHeigth * BallRadius));

	// Add material in case it was changed
	if (BallMaterial != NULL){
		BallCollision->SetMaterial(0, BallMaterial);
	}

	//BallMovement->Velocity = UKismetMathLibrary::GetDirectionUnitVector(FVector(-1.0f, -1.0f, 0.0f), FVector(1.0f, 1.0f, 0.0f));
	BallMovement->Velocity = GetRandDirection(BallStartMinAngle, BallStartMaxAngle);
	BallMovement->Velocity *= BallMovement->InitialSpeed;
}

// Called every frame
void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FVector ABall::GetRandDirection(float MinAngle, float MaxAngle)
{
	float Yaw = FMath::RandRange(MinAngle, MaxAngle);

	// Make 50% chance of making direction in oposite way [-MaxAngle, - MinAngle]
	if (FMath::RandRange(0, 1)) {
		Yaw += 180.0f;
	}

	FRotator Rotation(0.0f, Yaw, 0.0f);

	return Rotation.Vector();
}

