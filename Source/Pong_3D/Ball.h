// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "GameFramework/ProjectileMovementComponent.h"

#include "Ball.generated.h"

UCLASS()
class PONG_3D_API ABall : public AActor
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* BallCollision;

	// Ball diameter scale coefficient
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float BallRadius = 3.0f;

	// default ball mesh half heigth
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float BallDefaultHalfHeigth = 50.0f;

	// Ball movement component
	UPROPERTY(VisibleAnywhere)
		UProjectileMovementComponent* BallMovement;

	// Initial and max speed of the ball
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float BallSpeed = 1000.0f;

	// Material of the ball
	UPROPERTY(EditAnywhere)
		UMaterialInstance* BallMaterial;

	// Min angle where to shoot ball on the beginning
	UPROPERTY(EditAnywhere)
		float BallStartMinAngle = -60.0f;

	// Max angle where to shoot ball on the beginning
	UPROPERTY(EditAnywhere)
		float BallStartMaxAngle = 60.0f;

public:	
	// Sets default values for this actor's properties
	ABall();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	// Called to generate unit vector with random direction (in yaw axis).
	// Ranged with given angles [0; 180] BUT result may be mirrored with 50% chance.
	UFUNCTION(BlueprintPure)
		FVector GetRandDirection(float MinAngle=0.0f, float MaxAngle=180.0f);

};
