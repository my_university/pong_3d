// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class PONG_3D_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected:
	virtual void SetupInputComponent() override;

public:
	// Called on MoveRight axis event to move paddle on Y axis.
	UFUNCTION(BlueprintCallable)
		void MoveRight(float AxisValue);

	// Server-side function to handle input
	UFUNCTION(Server, Reliable)
		void Server_MoveRight(float AxisValue);

	bool Server_MoveRight_Validate(float AxisValue);
	void Server_MoveRight_Implementation(float AxisValue);
};
