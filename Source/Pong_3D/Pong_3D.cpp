// Copyright Epic Games, Inc. All Rights Reserved.

#include "Pong_3D.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Pong_3D, "Pong_3D" );
