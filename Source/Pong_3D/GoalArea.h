// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GoalArea.generated.h"

UCLASS()
class PONG_3D_API AGoalArea : public AActor
{
	GENERATED_BODY()

private:
	UPROPERTY(VisibleAnywhere)
		class UBoxComponent* Trigger;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		uint8 OponentID;

public:	
	// Sets default values for this actor's properties
	AGoalArea();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called OnOverlapBegin event
	UFUNCTION(BlueprintCallable)
	void OverlapBegin(	UPrimitiveComponent* OverlappedComponent, 
						AActor* OtherActor, 
						UPrimitiveComponent* OtherComponent, 
						int32 OtherBodyIndex, 
						bool bFromSweep, 
						const FHitResult& Hit	);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
