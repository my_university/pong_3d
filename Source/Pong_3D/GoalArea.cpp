// Fill out your copyright notice in the Description page of Project Settings.


#include "GoalArea.h"

#include "Pong_3DGameModeBase.h"
#include "Ball.h"

#include "Components/BoxComponent.h"

// Sets default values
AGoalArea::AGoalArea()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create default trigger volume
	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));

	// Attach to RootComponent
	Trigger->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AGoalArea::BeginPlay()
{
	Super::BeginPlay();
	
	Trigger->OnComponentBeginOverlap.AddDynamic(this, &AGoalArea::OverlapBegin);
}

void AGoalArea::OverlapBegin(	UPrimitiveComponent* OverlappedComponent, 
								AActor* OtherActor, 
								UPrimitiveComponent* OtherComponent, 
								int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit	)
{
	if (!HasAuthority()) {
		return;
	}

	ABall* IsBall = Cast<ABall>(OtherActor);

	if (IsBall) {

		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Blue, "GOAL!!!");
		}

		OtherActor->Destroy();

		// Spawn new ball if game is not finished
		APong_3DGameModeBase* GameMode = Cast<APong_3DGameModeBase>(GetWorld()->GetAuthGameMode());
		GameMode->SpawnTheBall();

		// Increase score
		GameMode->IncreasePlayerScore(OponentID);

		// Announce the score
		uint8 P1_Score = GameMode->GetPlayerScore(0);
		uint8 P2_Score = GameMode->GetPlayerScore(1);
		FString Message = "Score: " + FString::FromInt(P1_Score) + "-" + FString::FromInt(P2_Score);

		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Orange, Message);

		if (P1_Score >= GameMode->GetMaxSore() || P2_Score >= GameMode->GetMaxSore()) {
			Message = "Player " + FString::FromInt((P2_Score > P1_Score) + 1) + " wins!!!";
			GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, Message);

			GameMode->ResetScore();
		}
	}
}

// Called every frame
void AGoalArea::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

