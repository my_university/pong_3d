// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "PlayerPaddle.h"
#include "MyPlayerController.h"

#include "Pong_3DGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PONG_3D_API APong_3DGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
protected:
	// First Player Score
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		uint8 P1_Score = 0;
	
	// Second player score
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		uint8 P2_Score = 0;

	// Maximum score
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		uint8 MaxScore = 10;

	// Represent status if all players are connected
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsAllPlayersConnected = false;

public:
	// Called to spawn the ball in the center
	// (usually to begin the round)
	UFUNCTION(BlueprintCallable)
		void SpawnTheBall();

	// Called to increase player score
	UFUNCTION(BlueprintCallable)
		void IncreasePlayerScore(uint8 PlayerID);

	// Called to get player score
	UFUNCTION(BlueprintPure)
		uint8 GetPlayerScore(uint8 PlayerID);

	UFUNCTION(BlueprintCallable)
		void ResetScore() { P1_Score = 0; P2_Score = 0; }

	// Called to get Maximum score
	UFUNCTION(BlueprintPure)
		uint8 GetMaxSore() { return MaxScore; }

	// Called to set maximum score
	UFUNCTION(Blueprintcallable)
		void SetMaxScore(uint8 NewMaxScore) { MaxScore = NewMaxScore > 0 ? NewMaxScore : 1; }

	void WaitEveryoneToConnect();

protected:
	virtual void BeginPlay() override;
};
