// Copyright Epic Games, Inc. All Rights Reserved.


#include "Pong_3DGameModeBase.h"

#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"

#include "Ball.h"

void APong_3DGameModeBase::SpawnTheBall()
{
	GetWorld()->SpawnActor(ABall::StaticClass());
}

void APong_3DGameModeBase::IncreasePlayerScore(uint8 PlayerID)
{
	// Increase player's score. If reached max - end the game.
	switch (PlayerID)
	{
	case 0:
		++P1_Score;
		break;
	case 1:
		++P2_Score;
		break;
	default:
		break;
	}
}

uint8 APong_3DGameModeBase::GetPlayerScore(uint8 PlayerID)
{
	uint8 score = 0;

	switch (PlayerID)
	{
	case 0:
		score = P1_Score;
		break;
	case 1:
		score = P2_Score;
		break;
	default:
		break;
	}

	return score;
}

void APong_3DGameModeBase::WaitEveryoneToConnect()
{
	if (GetNumPlayers() == 2)
	{
		if (!IsAllPlayersConnected && HasAuthority())
		{
			IsAllPlayersConnected = true;
			SpawnTheBall();
		}
	}
	else {
		GEngine->AddOnScreenDebugMessage(1, 0.2f, FColor::Red, "Waiting all players to connect...");
	}
}

void APong_3DGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &APong_3DGameModeBase::WaitEveryoneToConnect, 0.2f, true);
	
	if (IsAllPlayersConnected) {
		GetWorldTimerManager().ClearTimer(TimerHandle);
	}
}
