// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerController.h"

#include "Net/UnrealNetwork.h"

#include "PlayerPaddle.h"

void AMyPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	
	//InputComponent->BindAxis("MoveRight", Cast<APlayerPaddle>(GetPawn()), &APlayerPaddle::MoveRight);
	InputComponent->BindAxis("MoveRight", this, &AMyPlayerController::MoveRight);
}

void AMyPlayerController::MoveRight(float AxisValue)
{
	APlayerPaddle* PlayerPawn = Cast<APlayerPaddle>(GetPawn());

	if (PlayerPawn != NULL && AxisValue != 0.0f) {
		if (HasAuthority()) {
			// Apply movement if server
			PlayerPawn->MoveRight(AxisValue);
		}
		else {
			Server_MoveRight(AxisValue);
			PlayerPawn->MoveRight(AxisValue);
		}
	}

	/*
	APlayerPaddle* PlayerPawn = Cast<APlayerPaddle>(GetPawn());

	if (PlayerPawn != NULL && AxisValue != 0.0f) {	
		PlayerPawn->MoveRight(AxisValue);
		
		//AController* Controller = PlayerPawn->GetController();

		//if ((Controller != nullptr) && (AxisValue != 0.0f))
		//{
		//	// Find out which way is right
		//	const FRotator Rotation = Controller->GetControlRotation();
		//	const FRotator YawRotation(0, Rotation.Yaw, 0);

		//	// Get right vector 
		//	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		//	// Add movement in that direction
		//	PlayerPawn->AddMovementInput(Direction, AxisValue);
		//}
		
	}
	*/

}

// Server-side function to handle input
void AMyPlayerController::Server_MoveRight_Implementation(float AxisValue)
{
	APlayerPaddle* PlayerPawn = Cast<APlayerPaddle>(GetPawn());

	if (PlayerPawn && FMath::Abs(AxisValue) > 0)
	{
		PlayerPawn->MoveRight(AxisValue);
	}
}

// Replicate the server-side input handling
bool AMyPlayerController::Server_MoveRight_Validate(float AxisValue)
{
	return true;
}