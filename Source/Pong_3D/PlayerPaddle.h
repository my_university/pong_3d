// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "GameFramework/PlayerStart.h"
#include "Camera/CameraComponent.h"
#include "Net/UnrealNetwork.h"

#include "PlayerPaddle.generated.h"

UCLASS()
class PONG_3D_API APlayerPaddle : public APawn
{
	GENERATED_BODY()

private:
	// The speed of the paddle
	UPROPERTY(EditAnywhere)
		float PaddleSpeed = 250.0f;

	// Default mesh height
	UPROPERTY(EditAnywhere)
		float MeshHeigth = 256.0f;

	// Paddle width
	UPROPERTY(VisibleAnywhere)
		float PaddleWidth;

	// Paddle location
	UPROPERTY(Replicated)
		FVector PaddleLocation;

protected:
	// Player's paddle
	UPROPERTY(EditDefaultsOnly)
		UStaticMeshComponent* Paddle;

	// Player's Camera
	UPROPERTY(EditDefaultsOnly)
		UCameraComponent* Camera;

public:
	// Sets default values for this pawn's properties
	APlayerPaddle();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Replicate the paddle's location to clients
	UFUNCTION()
		void OnRep_PaddleLocation();

	// Find the nearest player start
	UFUNCTION()
		APlayerStart* FindNearestPlayerStart();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/*
	*/
	// Called on MoveRight axis event
	UFUNCTION(BlueprintCallable)
		void MoveRight(float AxisValue);

};
