// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPaddle.h"

#include "GameFramework/PawnMovementComponent.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"

#include "MyPlayerController.h"

// Sets default values
APlayerPaddle::APlayerPaddle()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    // Create default RootComponent
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

    // Create default components
    Paddle = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlayerPaddle"));
    Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("PlayerCamera"));

    SetReplicates(true);
    SetReplicateMovement(true);

    // Setup Paddle mesh
    const ConstructorHelpers::FObjectFinder<UStaticMesh> PaddleMesh(TEXT("StaticMesh'/Engine/EngineMeshes/Cube.Cube'"));
    if (PaddleMesh.Succeeded()) {
        Paddle->SetStaticMesh(PaddleMesh.Object);
    }

    Paddle->SetRelativeScale3D(FVector(0.25f, 2.0f, 1.5f));
    PaddleWidth = MeshHeigth * Paddle->GetRelativeScale3D().Y;

    Paddle->SetRelativeLocation(FVector(0.0f, 0.0f, Paddle->GetRelativeScale3D().Z * MeshHeigth / 2));

    Paddle->SetIsReplicated(true);

    // Setup Camera positioning
    Camera->SetRelativeLocation(FVector(-1100.0f, 0.0f, 1150.0f));
    Camera->SetRelativeRotation(FRotator(-30.0f, 0.0f, 0.0f));

    // Attach to RootComponent
    Paddle->SetupAttachment(RootComponent);
    //RootComponent = Paddle;
    Camera->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APlayerPaddle::BeginPlay()
{
	Super::BeginPlay();
    
    // Set paddle width
    PaddleWidth = MeshHeigth * Paddle->GetRelativeScale3D().Y;

    APlayerStart* NearestStart = FindNearestPlayerStart();
    if (NearestStart)
    {
        PaddleLocation = NearestStart->GetActorLocation();
        SetActorLocation(PaddleLocation);
    }
}

void APlayerPaddle::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    DOREPLIFETIME(APlayerPaddle, PaddleLocation);
}

void APlayerPaddle::OnRep_PaddleLocation()
{
    SetActorLocation(PaddleLocation);
}

APlayerStart* APlayerPaddle::FindNearestPlayerStart()
{
    TArray<AActor*> PlayerStarts;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), PlayerStarts);

    APlayerStart* NearestStart = nullptr;
    float NearestDistance = FLT_MAX;

    for (AActor* Start : PlayerStarts)
    {
        float Distance = FVector::DistSquared(GetActorLocation(), Start->GetActorLocation());
        if (Distance < NearestDistance)
        {
            NearestStart = Cast<APlayerStart>(Start);
            NearestDistance = Distance;
        }
    }

    return NearestStart;
}

// Called every frame
void APlayerPaddle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPaddle::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

/*
*/
void APlayerPaddle::MoveRight(float AxisValue)
{
    /*
    if ((Controller != nullptr) && (AxisValue != 0.0f))
    {
        // Find out which way is right
        FRotator Rotation = Controller->GetControlRotation();
        FRotator YawRotation(0, Rotation.Yaw, 0);

        // Get right vector 
        FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
        Direction.Y *= AxisValue * PaddleSpeed;

        // Move component in that direction
        Paddle->MoveComponent(Direction, Rotation, true);
    }
    */

    FVector NewLocation = GetActorLocation();
    // Find out which way is right
    FRotator Rotation = Controller->GetControlRotation();
    FRotator YawRotation(0, Rotation.Yaw, 0);

    // Get right vector 
    FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

    NewLocation += FVector(0.0f, Direction.Y * AxisValue * PaddleSpeed * GetWorld()->GetDeltaSeconds(), 0.0f);

    // Perform a line trace to check for collisions
    FHitResult HitResult;
    FCollisionQueryParams CollisionParams;
    CollisionParams.AddIgnoredActor(this); // Ignore the current paddle

    FVector DestinationEdgeLeft = NewLocation + FVector(0.0f, -1.0f * PaddleWidth / 2, 0.0f);
    FVector DestinationEdgeRight = NewLocation + FVector(0.0f, PaddleWidth / 2, 0.0f);

    // Perform the line trace
    bool IsHit = GetWorld()->LineTraceSingleByChannel(
            HitResult,
            DestinationEdgeLeft,
            DestinationEdgeRight,
            ECC_Visibility,
            CollisionParams
        );
 

    // If there's no collision, move the paddle to the desired location
    if (!IsHit)
    {
        SetActorLocation(NewLocation);

        // Replicate the updated location to all clients
        PaddleLocation = NewLocation;
    }

    /*    
    TArray<AActor*> IsOverlapping;
    Paddle->GetOverlappingActors(IsOverlapping);
    
    if (IsOverlapping.Num() < 1){
        NewLocation += FVector(0.0f, Direction.Y * AxisValue * PaddleSpeed * GetWorld()->GetDeltaSeconds(), 0.0f);
    }
    else {
        GEngine->AddOnScreenDebugMessage(-1, 3.0, FColor::Red, "overlapping");
    }

    GEngine->AddOnScreenDebugMessage(-1, 3.0, FColor::Red, FString::FromInt(IsOverlapping.Num()));
    */

    /*
    if (NewLocation.Y < -1220) {
        NewLocation.Y = -1220;
    }
    else if (NewLocation.Y > 1220){
        NewLocation.Y = 1220;
    }
    */

    //SetActorLocation(NewLocation);

    // Replicate the updated location to all clients
    //PaddleLocation = NewLocation;
}

